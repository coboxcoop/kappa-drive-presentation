### [kappa-drive](https://gitlab.com/coboxcoop/kappa-drive)

Making a set of hyperdrives appear as one

[https://gitlab.com/coboxcoop/kappa-drive](https://gitlab.com/coboxcoop/kappa-drive)

---

### history

- originally ideated as [peerfs](https://github.com/okdistribute/peerfs) by [@okdistribute](https://github.com/okdistribute)
- picked up by [@ameba23](https://github.com/ameba23) and [@magpie](https://gitlab.com/m4gpie), completed the API, updated hypercore/hyperdrive, added conflict resolution mechanisms, developed as part of the CoBox project
- support from [@frando](https://github.com/frando) to upgrade to a [kappa-core version](https://github.com/frando/kappa-core/tree/sonar-0.4.2) decoupled from multifeed's storage system

---

### `kappa-drive = kappa-core + hyperdrive + multifeed`

- [hyperdrive](https://github.com/hypercore-protocol/hyperdrive) - filesystem abstraction built on two hypercores
- [hypercore](https://github.com/hypercore-protocol/hypercore) - an append only log with replication
- [multifeed](https://github.com/kappa-db/multifeed) - share a set of hypercores over a single topic / key
- [kappa-core](https://github.com/kappa-db/kappa-core) - materialised views built on top of hypercores
- as a great example of kappa-core in action - lets look at [cabal](https://cabal.chat/)

---

### how cabal works

![](./img/cabal-represent.png)

---

### how kappa-drive works

![](./img/kappa-drive-representation.png)

Using kappa-core we maintain an index of filenames mapped to a representation of causal state

---

Each peer's hyperdrive contains only the files they have made writes to

![](./img/kappa-drive-files.png)

<small>But we create a view of the current state of the archive</small>

---

### what happens when we do a write operation?

- Call `get` on our kappa index to retrieve our current knowledge on the state of the file
- Make the write to our own hyperdrive
- Create a `state` message with our subjective model of who did what
- Add this message to our metadata feed using `drive._update`

---

### `whoHasFile` - what happens when we do a read operation?
- Call `get` on our kappa index with the filename we are interested in
- If there is no entry, return our own drive
- Otherwise, decide which entry we think is the most up to date using our vector clock model
- If there is any additional doubt, select the latest writer by UNIX timestamp
- Return the winning hyperdrive
- Do the read operation on the returned drive 

---

### vector clock

![](./img/vector-clock.png)

Time measured logically in terms of number of events experienced

---

### FUSE mounts

- additional fuse wrapper called [kappa-drive-mount](https://gitlab.com/coboxcoop/kappa-drive-mount/)
- utilised [hyperdrive-fuse](https://github.com/andrewosh/hyperdrive-fuse) which binds to the FUSE API through [fuse-native](https://github.com/fuse-friends/fuse-native)
- creates a FUSE (file system in userspace) directory

---

### what went well

- Works well for archiving
- offline-first

---

### what went wrong

- Problems with FUSE
- It is not good for collaborative editing - there are better tools for this (eg: git)
- Duplication of data when updating a file
- Duplication when re-organising directory structure

---

### what could be next?

(just some ideas)

- sparse replication?
- deduplication using diffs?
- support for hyperdrive mounts?

---

### other approaches

- [automerge](https://github.com/automerge/automerge) / [hypermerge](https://github.com/automerge/hypermerge) - CRDTs 
- hypercore 10 / autobase - link back to the previous entry on another core
- [multi-hyperdrive](https://github.com/RangerMauve/multi-hyperdrive) groups together functionally independent hyperdrives

---

### our primary use case

![](./img/cobox-logo.svg)

[https://cobox.cloud](https://cobox.cloud)

[https://gitlab.com/coboxcoop](https://gitlab.com/coboxcoop)

